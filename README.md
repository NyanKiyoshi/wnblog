What is WNBlog?
===============

"WNBlog" is a simple blog written in Python 2 with the framework "Pyramid" under MIT license.

This blog only have the "admin" accounts, they have *all* rights. For now, users can't post comments just read, maybe later if needed.

How to use?
===========
Clone this repository:
```bash
git clone https://github.com/NyanKiyoshi/wnblog.git
```

Change dir to the new folder:
```bash
cd clone wnblog
```

Create a Python virtual environment:
```bash
virtualenv-2.7 --no-site-packages venv
```

In the same directory, we need to download and install requirements in development mode:
```bash
venv/bin/python setup.py develop
```

After it was done we need to initialize the database:
```bash
venv/bin/initialize_wn_blog_db development.ini
```

Now just run the server:
```bash
venv/bin/pserve development.ini
```
