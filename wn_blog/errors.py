# -*- coding: utf-8 -*-
# ==== WNBlog
# AUTHOR: NyanKiyoshi
# COPYRIGHT: (c) 2014 - NyanKiyoshi
# URL: https://github.com/NyanKiyoshi/WNBlog/
# LICENSE: https://github.com/NyanKiyoshi/WNBlog/LICENSE
#
# This file is part of WNBlog is under the MIT license. Please take awareness about this latest before doing anything!

db_unreachable = """
    <h1>Oops!</h1>
        <p>I can't reach the database. Please retry later, or try to contact the website's administrator.</p>
        <p>Sorry about that...</p>
"""
