# -*- coding: utf-8 -*-
# ==== WNBlog
# AUTHOR: NyanKiyoshi
# COPYRIGHT: (c) 2014 - NyanKiyoshi
# URL: https://github.com/NyanKiyoshi/WNBlog/
# LICENSE: https://github.com/NyanKiyoshi/WNBlog/LICENSE
#
# This file is part of WNBlog is under the MIT license. Please take awareness about this latest before doing anything!

from sqlalchemy.exc import OperationalError
from wn_blog.models import DBSession, AdminAccount


class AuthenticationPolicy(object):
    def authenticated_userid(self, request):
        return request.user

    def unauthenticated_userid(self, request):
        return None

    def effective_principals(self, request):
        if not request.user:
            return []
        perm = []
        if request.user:
            perm.append('admin')
        return perm


class AuthorizationPolicy(object):
    def permits(self, context, principals, permission):
        return permission in principals


def user(request):
    try:
        assert 'uid' in request.session
        assert type(request.session['uid']) == int
        assert 'token' in request.session
    except AssertionError:
        request.session['uid'] = ''
        return
    try:
        adm = DBSession.query(AdminAccount).filter_by(id=request.session['uid']).first()
    except OperationalError:
        del request.session['uid']  # unable to check...
        return
    if adm is not None:
        if adm.token == request.session['token']:
            return True
    return