# -*- coding: utf-8 -*-
# ==== WNBlog
# AUTHOR: NyanKiyoshi
# COPYRIGHT: (c) 2014 - NyanKiyoshi
# URL: https://github.com/NyanKiyoshi/WNBlog/
# LICENSE: https://github.com/NyanKiyoshi/WNBlog/LICENSE
#
# This file is part of WNBlog is under the MIT license. Please take awareness about this latest before doing anything!

from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from pyramid.httpexceptions import HTTPMovedPermanently
from pyramid_beaker import session_factory_from_settings
from wn_blog.security import *
from views.errors import unauthorized, bad_request, failed_dependency
from wn_blog.utils import load_settings

from .models import (
    DBSession,
    Base,
)


def add_http_exceptions(config):
    config.add_view(unauthorized, context='pyramid.httpexceptions.HTTPUnauthorized', renderer='errors/401.mako')
    config.add_view(bad_request, context='pyramid.httpexceptions.HTTPBadRequest', renderer='errors/400.mako')
    config.add_view(
        failed_dependency, context='pyramid.httpexceptions.HTTPFailedDependency', renderer='errors/424.mako'
    )
    config.add_view(
        failed_dependency, context='pyramid.httpexceptions.HTTPInternalServerError', renderer='errors/424.mako'
    )


def add_route(config, name, pattern, **kw):
    """ This function detect and redirect the user if he types an address ending with slash instead of without.
    """
    config.add_route(name, pattern, **kw)

    if not pattern.endswith('/'):
        config.add_route(name + '_', pattern + '/')

        def redirect(request):
            return HTTPMovedPermanently(request.route_url(name, _query=request.GET, **request.matchdict))
        config.add_view(redirect, route_name=name + '_')


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    session_factory = session_factory_from_settings(settings)

    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    config = Configurator(
        settings=settings,
        authentication_policy=AuthenticationPolicy(),
        authorization_policy=AuthorizationPolicy(),
    )
    config.add_request_method(user, 'user', reify=True, property=True)
    config.add_request_method(load_settings, 'settings', reify=True)
    config.set_session_factory(session_factory)
    config.include('pyramid_mako')
    config.add_static_view('static', 'static', cache_max_age=3600)

    add_http_exceptions(config)

    add_route(config, 'home', '/')
    add_route(config, 'manage', '/manage')
    add_route(config, 'manage_login', '/manage/login')
    add_route(config, 'logout', '/logout')
    add_route(config, 'manage_posts', '/manage/posts')
    add_route(config, 'add_post', '/manage/add')
    add_route(config, 'edit_post', '/manage/edit/{post_id:[0-9]{,64}}')
    add_route(config, 'delete_post', '/manage/delete/{post_id:[0-9]{,64}}')
    add_route(config, 'get_post', '/{call_url:[a-zA-Z0-9-_.]{1,255}}')

    config.scan()

    return config.make_wsgi_app()
