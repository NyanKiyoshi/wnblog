## -*- coding: utf-8 -*-
<%
    from wn_blog.utils import str_date

    gmt = 0
    if request.settings is not None:
        if 'gmt' in request.settings:
            gmt = request.settings['gmt']
%>
% if len(posts[:]) > 0:
    % for post in posts:
        <article>
            <h2>
                <a href="${post.url}">${post.title}</a>
            </h2>
            <div class="date">
                Published on
                % if post.pub_date:
                    ${str_date(post.pub_date, gmt)}.
                % else:
                    [?]
                % endif
            </div>
            <div class="content">
                <p>${post.excerpt}</p>
            </div>
        </article>
    % endfor
    % if current_page < pages:
        <a href="?page=${current_page + 1}">next</a>
    % endif
    % if 1 < current_page <= pages:
        <a href="?page=${current_page - 1}">previous</a>
    % endif
% else:
    <article>
        <p style="font-style:oblique">Oops... Good job sir, there is no article yet.</p>
    </article>
% endif

<%inherit file="base.mako"/>
<%block name="title">Home ~ WMBlog</%block>
<%block name="menu">
        % if request.user:
            <nav>
                <ul>
                    <li><a href="/manage">Manage</a></li>
                    <li><a href="/manage/add">New Article</a></li>
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </nav>
        % else:
            <nav>
                <ul>
                    <li><a href="/" class="active">Home</a></li>
                </ul>
            </nav>
        % endif
</%block>