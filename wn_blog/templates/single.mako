## -*- coding: utf-8 -*-
<%
    from wn_blog.utils import str_date

    gmt = 0
    if request.settings is not None:
        if 'gmt' in request.settings:
            gmt = request.settings['gmt']
%>
<article>
    <h2>
        <a href="${post['url']}">${post['title']}</a>
    </h2>
    <div class="date">
        % if post['pub_date']:
            Published on ${str_date(post['pub_date'], gmt)}
        % else:
            never published
        % endif
    </div>
    <div class="content">
        <p>
            % for c in post['content']:
                ${c.decode("utf-8", "replace") | n}
            % endfor
        </p>
    </div>
</article>

<%inherit file="base.mako"/>
<%block name="title">${post['title']} ~ WMBlog</%block>
<%block name="menu">
        % if request.user:
            <nav>
                <ul>
                    <li><a href="/manage">Manage</a></li>
                    <li><a href="/manage/add">New Article</a></li>
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </nav>
        % else:
            <nav>
                <ul>
                    <li><a href="/">Home</a></li>
                </ul>
            </nav>
        % endif
</%block>