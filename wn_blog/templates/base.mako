## -*- coding: utf-8 -*-
<!doctype HTML>

<html>
    <head>
        <title><%block name="title" /></title>
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, initial-scape=1" />
        <meta name="description" content="<%block name='description' />" />
        <link rel="stylesheet" type="text/css" href="${request.static_url('wn_blog:static/css/reset.css')}" />
        <link rel="stylesheet" type="text/css" href="${request.static_url('wn_blog:static/css/style.css')}" />
        <%block name="head" />
    </head>
<body>
    <header>
        <div class="title">
            <h1><a class="logo" href="/">WNBlog</a></h1>
        </div>
        <%block name="menu" />
    </header>


        % for f in request.session.pop_flash():
            % if f[0] == 'info':
                <div class="notify info">
                    ${f[1]}
                </div>
            % endif

            % if f[0] == 'warn':
                <div class="notify warning">
                    ${f[1]}
                </div>
            % endif

            % if f[0] == 'error':
                <div class="notify error">
                    ${f[1]}
                </div>
            % endif
        % endfor

            <section>
${self.body()}
            </section>
        <footer>
                DocoPress - A minimalist blog template by <a href="http://theoti.me">Théotix</a> - On <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons</a>
                <%block name="footer" />
        </footer>
    </body>
</html>
