## -*- coding: utf-8 -*-
<%
    from wn_blog.utils import compact_str_date

    gmt = 0
    if request.settings is not None:
        if 'gmt' in request.settings:
            gmt = request.settings['gmt']
%>

<article>
    <h2>Posts</h2>
        <div class="content">
            % if len(posts[:]) > 0:
                    <table>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Creation date</th>
                                <th>Publication date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            % for post in posts:
                                <tr>
                                    <td>${post.id}</td>
                                    <td><a href="/${post.url}">${post.title}</a></td>
                                    <td>
                                        % if post.published:
                                            <span class="green">published</span>
                                        % else:
                                            <span class="red">not published</span>
                                        % endif
                                    </td>
                                    <td>${compact_str_date(post.creation_date, gmt)}</td>
                                    <td>
                                        % if post.pub_date:
                                            ${compact_str_date(post.pub_date, gmt)}
                                        % else:
                                            never published
                                        % endif
                                        </td>
                                    <td>
                                        <a href="/manage/edit/${post.id}">edit</a>/
                                        <a href="/manage/delete/${post.id}">delete</a></td>
                                </tr>
                            % endfor
                        </tbody>
                    </table>
                    % if current_page < pages:
                        <a href="?page=${current_page + 1}">next</a>
                    % endif
                    % if 1 < current_page <= pages:
                        <a href="?page=${current_page - 1}">previous</a>
                    % endif
            % endif
        </div>
</article>

<%inherit file="../base.mako"/>
<%block name="title">Posts ~ WMBlog</%block>
<%block name="head">
    <link rel="stylesheet" type="text/css" href="${request.static_url('wn_blog:static/css/manage.css')}" />
</%block>
<%block name="menu">
        % if request.user:
            <nav>
                <ul>
                    <li><a href="/manage" class="active">Manage</a></li>
                    <li><a href="/manage/add">New Article</a></li>
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </nav>
        % else:
            <nav>
                <ul>
                    <li><a href="/">Home</a></li>
                </ul>
            </nav>
        % endif
</%block>
