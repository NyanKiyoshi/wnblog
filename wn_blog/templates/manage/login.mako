## -*- coding: utf-8 -*-
<article>
    <h2>Login</h2>
    <div class="content">
        <form method="post">
            <div>
                <label for="identifier">Identifier:</label>
                <div>
                    <input type="text" name="identifier" id="identifier" value="${identifier}" />
                </div>
            </div>

            <div>
                <label for="password">Password:</label>
                <div>
                    <input type="password" name="password" id="password" />
                </div>
            </div>

            <div>
                <input type="submit" />
            </div>
        </form>
    </div>
</article>

<%inherit file="../base.mako"/>
<%block name="title">Login ~ WMBlog</%block>
<%block name="head">
    <link rel="stylesheet" type="text/css" href="${request.static_url('wn_blog:static/css/form.css')}" />
</%block>
<%block name="menu">
        % if request.user:
            <nav>
                <ul>
                    <li><a href="/manage" class="active">Manage</a></li>
                    <li><a href="/manage/add">New Article</a></li>
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </nav>
        % else:
            <nav>
                <ul>
                    <li><a href="/">Home</a></li>
                </ul>
            </nav>
        % endif
</%block>
