## -*- coding: utf-8 -*-
<article>
    <h2>${post['title'] and 'Editing "' + post['title'] + '".' or 'Adding a new article.'}</h2>
    <div class="content">
        <form method="post">
            <label for="title">Title:</label>
            <div>
                <input type="text" name="title" id="title" value="${post['title']}" />
            </div>

            <label for="url">Call url:</label>
            <div>
                <input type="text" name="url" id="url" value="${post['url']}" />
            </div>

            <label for="content">Content:</label>
            <div>
                <textarea name="content" id="content" rows="30">${post['content']}</textarea>
            </div>

            <label for="excerpt">Excerpt:</label>
            <div>
                <textarea name="excerpt" id="excerpt" rows="7">${post['excerpt']}</textarea>
            </div>

            <label for="published">Published:</label>
                <input
                        type="checkbox"
                        name="published"
                        id="published"
                        % if post['published']:
                            checked
                        % endif
                />

            <div>
                <input type="submit" />
            </div>
        </form>
    </div>
</article>

<%inherit file="../base.mako"/>
<%block name="title">Edit ${post['title'] and '"' + post['title'] + '"' or ''} ~ WMBlog</%block>
<%block name="head">
    <link rel="stylesheet" type="text/css" href="${request.static_url('wn_blog:static/css/form.css')}" />
</%block>
<%block name="menu">
        % if request.user:
            <nav>
                <ul>
                    % if post['edit']:
                        <li><a href="/manage" class="active">Manage</a></li>
                        <li><a href="/manage/add">New Article</a></li>
                    % else:
                        <li><a href="/manage">Manage</a></li>
                        <li><a href="/manage/add" class="active">New Article</a></li>
                    % endif
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </nav>
        % else:
            <nav>
                <ul>
                    <li><a href="/">Home</a></li>
                </ul>
            </nav>
        % endif
</%block>
