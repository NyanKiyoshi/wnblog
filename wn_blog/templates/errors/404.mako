<article>
    <h2>404: meow not found!</h2>
    <div class="content">
        <h3>This bellow cat is sorry about that...</h3>
        <img src="${request.static_url('wn_blog:static/errors/404.jpg')}" alt="The cat is not found too!"/>
        <p>
            <span title="Source">Sauce</span>:
            <a href="https://www.flickr.com/photos/girliemac/6508022985/">@girliemac</a>
            on <a href="https://flickr.com">flickr.com</a>
            (<a href="https://creativecommons.org/licenses/by/2.0/">CC BY 2.0</a>).
        </p>
    </div>
</article>

<%inherit file="../base.mako"/>
<%block name="title">Meow not found into WMBlog</%block>