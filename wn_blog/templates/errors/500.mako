<article>
    <h2>424: we are not able to find the fishes...</h2>
    <div class="content">
        <h3>The icehouse looks like doesn't contain the requirements...</h3>
        <img src="${request.static_url('wn_blog:static/errors/500.jpg')}" alt="The cat has been successfully escaped..."/>
        <p>
            <span title="Source">Sauce</span>:
            <a href="https://www.flickr.com/photos/girliemac/6508022985/">@girliemac</a>
            on <a href="https://flickr.com">flickr.com</a>
            (<a href="https://creativecommons.org/licenses/by/2.0/">CC BY 2.0</a>).
        </p>
    </div>
</article>

<%inherit file="../base.mako"/>
<%block name="title">We are not able to find the fishes onto WNBlog...</%block>