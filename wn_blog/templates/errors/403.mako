<article>
    <h2>403: cats are forbidden!</h2>
    <div class="content">
        <h3>Wait... What. Dogs are not allowed, but cats are allowed...</h3>
        <img src="${request.static_url('wn_blog:static/errors/403.jpg')}" alt="You can't load the cat too."/>
        <p>
            <span title="Source">Sauce</span>:
            <a href="https://www.flickr.com/photos/girliemac/6508023617/">@girliemac</a>
            on <a href="https://flickr.com">flickr.com</a>
            (<a href="https://creativecommons.org/licenses/by/2.0/">CC BY 2.0</a>).
        </p>
    </div>
</article>

<%inherit file="../base.mako"/>
<%block name="title">Cats are forbidden into WMBlog</%block>