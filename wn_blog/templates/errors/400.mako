<article>
    <h2>401: holy meow is unauthorized!</h2>
    <div class="content">
        <h3>The bellow door is sorry about that...</h3>
        <img src="${request.static_url('wn_blog:static/errors/401.jpg')}" alt="The cat looks like invisible..."/>
        <p>
            <span title="Source">Sauce</span>:
            <a href="https://www.flickr.com/photos/girliemac/6514584423/">@girliemac</a>
            on <a href="https://flickr.com">flickr.com</a>
            (<a href="https://creativecommons.org/licenses/by/2.0/">CC BY 2.0</a>).
        </p>
    </div>
</article>

<%inherit file="../base.mako"/>
<%block name="title">Holy meow is unauthorized into WMBlog</%block>