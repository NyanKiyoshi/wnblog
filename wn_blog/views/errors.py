# -*- coding: utf-8 -*-
# ==== WNBlog
# AUTHOR: NyanKiyoshi
# COPYRIGHT: (c) 2014 - NyanKiyoshi
# URL: https://github.com/NyanKiyoshi/WNBlog/
# LICENSE: https://github.com/NyanKiyoshi/WNBlog/LICENSE
#
# This file is part of WNBlog is under the MIT license. Please take awareness about this latest before doing anything!

from pyramid.view import (
    notfound_view_config,
    forbidden_view_config,

)
from pyramid.url import route_url


@notfound_view_config(renderer='errors/404.mako')
def not_found(request):
    request.response.status = 404
    return {}


@forbidden_view_config(renderer='errors/403.mako')
def forbidden(request):
    request.response.status = 403
    return {}


def unauthorized(request):
    request.response.status = 401
    return {}


def bad_request(request):
    request.response.status = 400
    return {}


def failed_dependency(request):
    request.response.status = 424
    return {}


def internal_server_error(request):
    request.response.status = 500
    return {}