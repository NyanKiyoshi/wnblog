# -*- coding: utf-8 -*-
# ==== WNBlog
# AUTHOR: NyanKiyoshi
# COPYRIGHT: (c) 2014 - NyanKiyoshi
# URL: https://github.com/NyanKiyoshi/WNBlog/
# LICENSE: https://github.com/NyanKiyoshi/WNBlog/LICENSE
#
# This file is part of WNBlog is under the MIT license. Please take awareness about this latest before doing anything!

from pyramid.view import view_config
from wn_blog.models import *
from wn_blog.utils import check_attempts, new_token, flash_response
from pyramid.httpexceptions import HTTPFound, HTTPUnauthorized, HTTPBadRequest, HTTPNotFound, HTTPInternalServerError
from datetime import timedelta, datetime
from transaction import manager
from random import randrange
from sqlalchemy.exc import OperationalError
from wn_blog.errors import *
from pyramid.response import Response
from math import ceil
from os import remove
from os.path import dirname, abspath
from shutil import copyfile
from re import match


@view_config(route_name='manage_login', renderer='manage/login.mako')
def login(request):
    """
    An invalid ID or and an invalid password is treat like a login fail, even if the account doesn't exist.
    If the IP login attempts in 24 hours is > 7 -> raise 401 (any account)

    The ID must respect the case, if the user type "hello" instead of "Hello" the ID will be wrong.
    """
    print request.session
    if request.user:
        return HTTPFound(location=request.route_url('manage'))
    if request.method == 'POST':
        post = {}
        for key in ('identifier', 'password'):
            try:
                post[key] = request.POST[key] if len(request.POST[key]) < 255 else ''
            except KeyError:
                raise HTTPBadRequest
        try:
            attempts = check_attempts(request.remote_addr)
            if attempts > 7:
                if attempts > 8:
                    instance = DBSession.query(LoginAttempt).filter_by(
                        remote_address=request.remote_addr
                    ).filter(LoginAttempt.date >= datetime.utcnow() - timedelta(hours=24)).slice(8, attempts)
                    if instance:
                        with manager:
                            for x in instance:
                                try:
                                    DBSession.delete(x)
                                except OperationalError:
                                    pass
                raise HTTPUnauthorized()
            del attempts

            adm = DBSession.query(AdminAccount).filter_by(identifier=post['identifier']).first()
            if adm is None:
                with manager:
                    DBSession.add(
                        LoginAttempt(
                            admin_id=0,
                            remote_address=request.remote_addr,
                        )
                    )
                flash_response(request, 'Invalid ID or password.')
                return {'identifier': post['identifier']}
            if not adm.check_password(post['password']):
                DBSession.add(
                    LoginAttempt(
                        admin_id=0,
                        remote_address=request.remote_addr,
                    )
                )
                flash_response(request, 'Invalid ID or password.')
                return {'identifier': post['identifier']}

            for x in DBSession.query(LoginAttempt).filter_by(remote_address=request.remote_addr):
                try:
                    DBSession.delete(x)
                except OperationalError:
                    pass

            token = new_token(randrange(255, 500))
            request.session['uid'], request.session['token'] = adm.id, token
            with manager:
                adm.token = token
                DBSession.add(adm)
            flash_response(request, 'Logged in!', 2)
        except OperationalError:
            return Response(db_unreachable)
        return HTTPFound(location=request.route_url('manage'))
    return {'identifier': ''}


@view_config(route_name='logout', permission='admin')
def logout(request):
    for key in request.session.keys():
        del request.session[key]
    return HTTPFound(location=request.route_url('home'))


@view_config(route_name='manage', renderer='manage/index.mako', permission='admin')
def manage(request):
    by_page = 50
    if 'page' in request.params:
        try:
            p = int(request.params.get('page'))
            p = p if 1 <= p <= 100 else 1
        except ValueError:
            p = 1
    else:
        p = 1

    try:
        posts = {
            'total': DBSession.query(Post).count(),
            'current_page': p,
            'pages': 1,
            'posts': None,
        }
        del p

        posts['pages'] = ceil(posts['total'] / by_page)

        posts['posts'] = DBSession.query(Post).order_by("pub_date desc").slice(
            (posts['current_page'] - 1) * by_page,
            ceil(posts['current_page'] * by_page)
        )
    except OperationalError:
        request.response.status = 500
        return Response(db_unreachable)
    return posts


@view_config(route_name='edit_post', renderer='manage/edit.mako', permission='admin')
def edit(request):
    """
    Edit a given post. The previous post content is duplicated as {id}.html.bak.
    """
    post_id = request.matchdict['post_id']
    try:
        _post = DBSession.query(Post).filter_by(id=post_id).first()
    except OperationalError:
        request.response.status = 500
        return Response(db_unreachable)

    if _post is None:
        raise HTTPNotFound()

    root = dirname(dirname(abspath(__file__)))

    if not request.method == 'POST':
        post = {
            'edit': True,
            'id': post_id,
            'url': _post.url.lower(),
            'title': _post.title,
            'excerpt': _post.excerpt,
            'published': _post.published,
        }

        try:
            with open(root + '/posts/%s.html' % post_id, 'r') as o:
                post['content'] = o.readlines()
                if type(post['content']) is list:
                    content = ''
                    # otherwise Mako template adds many blank lines.
                    for c in post['content']:
                        content += c.decode("utf-8", "replace")
                    post['content'] = content
                    del content
        except IOError as e:
            post['content'] = '{0}: {1}'.format(type(e), e)
        return {'post': post}

    params = {
        'edit': True,
        'url': str,
        'title': str,
        'content': str,
        'excerpt': str,
    }
    for key in ('url', 'title', 'content', 'excerpt'):
        try:
            params[key] = request.params[key]
        except KeyError:
            raise HTTPBadRequest

    params['published'] = False if 'published' not in request.params else True

    error = False
    error = flash_response(request, 'The content must more than 10 characters.') \
        if not 10 <= len(params['content']) <= 800000 else error
    error = flash_response(
        request,
        'The url must be between 1 and 255 characters and must contain only alphanumerics characters and or'
        'these special characters: "-_.".'
    ) \
        if not match('^[a-zA-Z0-9_.-]{,255}$', params['url']) else error
    error = flash_response(request, 'The title must be between 1 and 140 characters.') \
        if not 1 <= len(params['title']) <= 140 else error
    error = flash_response(request, 'The excerpt must be between 1 and 500 characters.') \
        if not 1 <= len(params['excerpt']) <= 500 else error
        #if not 1 <= len(params['excerpt']) <= 500 and params['excerpt'] != '' else error

    if error:
        return {'post': params}

    #if params['excerpt'] == '':
    #    params['excerpt'] = params['content'][:369] + ' [...]'

    try:
        url = DBSession.query(Post.id).filter_by(url=request.params['url'])
        if len(url[:]) > 0 and _post.url != request.params['url']:
            flash_response(request, 'The url "{0}" is already taken.'.format(request.params['url']))
            return {'post': params}
        del url
    except OperationalError:
        return Response(db_unreachable(request))

    _post.url, _post.title, _post.excerpt = params['url'].lower(), params['title'], params['excerpt']
    _post.published = params['published']

    if params['published'] and _post.pub_date is None:
        _post.pub_date = datetime.utcnow()

    try:
        copyfile(root + '/posts/%s.html' % post_id, root + '/posts/%s.html.old' % post_id)
        with open(root + '/posts/%s.html' % post_id, 'w') as o:
            o.write(params['content'].encode("utf-8", "replace"))
    except IOError as e:
        done = False
        if e[0] == 2:
            try:
                with open(root + '/posts/%s.html' % post_id, 'a') as o:
                    o.write(params['content'].encode("utf-8", "replace"))
                done = True
            except IOError:
                pass
        if not done:
            raise HTTPInternalServerError()
        else:
            pass

        with manager:
            try:
                DBSession.add(_post)
            except OperationalError:
                return Response(db_unreachable(request))
    return HTTPFound(location=request.route_url('get_post', call_url=params['url']))


@view_config(route_name='add_post', renderer='manage/edit.mako', permission='admin')
def add(request):
    if not request.method == 'POST':
        return {
            'post': {
                'edit': False,
                'url': '',
                'title': '',
                'excerpt': '',
                'content': '',
                'published': False,
            }
        }

    root = dirname(dirname(abspath(__file__)))

    params = {
        'edit': False,
        'url': str,
        'title': str,
        'content': str,
        'excerpt': str,
    }
    for key in ('url', 'title', 'content', 'excerpt'):
        try:
            params[key] = request.params[key]
        except KeyError:
            raise HTTPBadRequest

    params['url'] = params['url'].lower()

    params['published'] = False if 'published' not in request.params else True

    error = False
    error = flash_response(request, 'The content must more than 10 characters.') \
        if not 10 <= len(params['content']) <= 800000 else error
    error = flash_response(
        request,
        'The url must be between 1 and 255 characters and must contain only alphanumerics characters and or'
        'these special characters: "-_.".'
    ) \
        if not match('^[a-zA-Z0-9_.-]{,255}$', params['url']) else error
    error = flash_response(request, 'The title must be between 1 and 140 characters.') \
        if not 1 <= len(params['title']) <= 140 else error
    error = flash_response(request, 'The excerpt must be between 1 and 500 characters.') \
        if not 1 <= len(params['excerpt']) <= 500 else error
        #if not 1 <= len(params['excerpt']) <= 500 and params['excerpt'] != '' else error

    if error:
        return {'post': params}

    #if params['excerpt'] == '':
    #    params['excerpt'] = params['content'][:369] + ' [...]'

    try:
        url = DBSession.query(Post.id).filter_by(url=request.params['url'])
        if len(url[:]) > 0:
            flash_response(request, 'The url "{0}" is already taken.'.format(request.params['url']))
            return {'post': params}
        del url
    except OperationalError:
        return Response(db_unreachable(request))

    with manager:
        try:
            post = Post(
                url=params['url'].lower(),
                title=params['title'],
                excerpt=params['excerpt'],
                published=params['published'],
            )
            if params['published']:
                post.pub_date = datetime.utcnow()
            DBSession.add(post)
            DBSession.flush()

            try:
                with open(root + '/posts/%s.html' % post.id, 'w') as o:
                    o.write(params['content'].encode("utf-8", "replace"))
            except IOError:
                post.published = False
                try:
                    DBSession.add(post)
                except OperationalError:
                    pass
                raise HTTPInternalServerError()

        except OperationalError:
            return Response(db_unreachable(request))

    return HTTPFound(location=request.route_url('get_post', call_url=params['url']))


@view_config(route_name='delete_post', permission='admin')
def delete(request):
    if request.settings is not None:
        if 'can_delete' in request.settings:
            if not request.settings['can_delete']:
                return Response('ERROR: functionality disabled.')
        else:
            return Response('ERROR: please enable (or disable) this functionality in the settings.')
    else:
        return Response('ERROR: invalid configuration file.')
    post_id = request.matchdict['post_id']
    try:
        _post = DBSession.query(Post).filter_by(id=post_id).first()
        if _post is None:
            raise HTTPNotFound()
        DBSession.delete(_post)
    except OperationalError:
        request.response.status = 500
        return Response(db_unreachable)

    root = dirname(dirname(abspath(__file__)))
    try:
        remove(root + '/posts/%s.html' % post_id)
    except IOError as e:
        flash_response(request, 'The post has been removed but the file still exist... [ERROR CODE: %s]' % e[0], 1)
    except OSError as e:
        flash_response(request, 'The post has been removed but the file still exist... [ERROR CODE: %s]' % e[0], 1)
    else:
        flash_response(request, 'The post has been removed.')
    return HTTPFound(location=request.route_url('manage'))
