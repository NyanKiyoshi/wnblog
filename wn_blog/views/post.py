# -*- coding: utf-8 -*-
# ==== WNBlog
# AUTHOR: NyanKiyoshi
# COPYRIGHT: (c) 2014 - NyanKiyoshi
# URL: https://github.com/NyanKiyoshi/WNBlog/
# LICENSE: https://github.com/NyanKiyoshi/WNBlog/LICENSE
#
# This file is part of WNBlog is under the MIT license. Please take awareness about this latest before doing anything!

from pyramid.view import view_config
from wn_blog.models import *
from math import ceil
from sqlalchemy.exc import OperationalError
from wn_blog.errors import *
from pyramid.response import Response
from pyramid.httpexceptions import HTTPNotFound, HTTPFailedDependency
from os.path import dirname, abspath


@view_config(route_name='home', renderer='home.mako')
def home(request):
    by_page = 10
    if 'page' in request.params:
        try:
            p = int(request.params.get('page'))
            p = p if 1 <= p <= 100 else 1
        except ValueError:
            p = 1
    else:
        p = 1

    try:
        posts = {
            'total': DBSession.query(Post).filter_by(published=True).count(),
            'current_page': p,
            'pages': 1,
            'posts': None,
        }
        del p

        posts['pages'] = ceil(posts['total'] / by_page)

        posts['posts'] = DBSession.query(Post).filter_by(published=True).order_by("pub_date desc").slice(
            (posts['current_page'] - 1) * by_page,
            ceil(posts['current_page'] * by_page)
        )
    except OperationalError:
        request.response.status = 500
        return Response(db_unreachable)

    return posts


@view_config(route_name='get_post', renderer='single.mako')
def get_post(request):
    call_url = str(request.matchdict['call_url']).lower()
    try:
        _post = DBSession.query(
            Post.id,
            Post.title,
            Post.pub_date,
            Post.url,
            Post.published,
        ).filter_by(url=call_url).first()
    except OperationalError:
        request.response.status = 500
        return Response(db_unreachable)

    if _post is None or (not _post.published and not request.user):
        raise HTTPNotFound()

    post = {
        'title': _post.title,
        'pub_date': _post.pub_date,
        'url': _post.url,
    }

    try:
        with open(dirname(dirname(abspath(__file__))) + '/posts/%s.html' % _post.id, 'r') as o:
            post['content'] = o.readlines()
    except IOError:
        raise HTTPFailedDependency
    return {'post': post}
