# -*- coding: utf-8 -*-
# ==== WNBlog
# AUTHOR: NyanKiyoshi
# COPYRIGHT: (c) 2014 - NyanKiyoshi
# URL: https://github.com/NyanKiyoshi/WNBlog/
# LICENSE: https://github.com/NyanKiyoshi/WNBlog/LICENSE
#
# This file is part of WNBlog is under the MIT license. Please take awareness about this latest before doing anything!

from datetime import datetime, timedelta
from wn_blog.models import DBSession, LoginAttempt
from random import choice
from os.path import dirname, abspath, exists
from json import loads


def load_settings(request):
    path = dirname(__file__) + '/settings.json'
    if exists(path):
        try:
            return loads(open(path, 'r').read())
        except IOError:
            # Unable to read the file...
            pass
        except ValueError:
            # Invalid syntax of the configuration file certainly.
            pass


def check_attempts(remote_address=None, admin_id=None):
    if remote_address:
        return DBSession.query(LoginAttempt)\
            .filter_by(remote_address=remote_address)\
            .filter(LoginAttempt.date >= datetime.utcnow() - timedelta(hours=24))\
            .count()
    elif admin_id:
        return DBSession.query(LoginAttempt)\
            .filter_by(admin_id=admin_id)\
            .filter(LoginAttempt.date >= datetime.utcnow() - timedelta(days=30.5))\
            .count()


def new_token(length, salt='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'):
    return ''.join([choice(salt) for i in range(length)])


def flash_response(request, message, level=0):
    if level == 2:
        request.session.flash(('info', message))
    elif level == 1:
        request.session.flash(('warn', message))
    else:
        request.session.flash(('error', message))
    return True


def str_date(utc_date, gmt=0):
    utc_date += timedelta(hours=gmt)
    return '{0:%A} {0:%d}, {0:%B}, {0:%Y} (GMT+{GMT})'.format(utc_date, GMT=gmt)


def compact_str_date(utc_date, gmt=0):
    utc_date += timedelta(hours=gmt)
    return '{0:%d}-{0:%m}-{0:%y} {0:%H}:{0:%M} [+{GMT}]'.format(utc_date, GMT=gmt)