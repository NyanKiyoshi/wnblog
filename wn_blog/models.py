# -*- coding: utf-8 -*-
# ==== WNBlog
# AUTHOR: NyanKiyoshi
# COPYRIGHT: (c) 2014 - NyanKiyoshi
# URL: https://github.com/NyanKiyoshi/WNBlog/
# LICENSE: https://github.com/NyanKiyoshi/WNBlog/LICENSE
#
# This file is part of WNBlog is under the MIT license. Please take awareness about this latest before doing anything!

from sqlalchemy import (
    Column,
    Integer,
    Text,
    Boolean,
    String,
    DateTime,
)

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
)

from zope.sqlalchemy import ZopeTransactionExtension
from datetime import datetime
from base64 import b64encode
from random import choice
import crypt

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()
SALT = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
SECRET_PASS = 'TODO'


class Post(Base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True)
    title = Column(Text(140))
    published = Column(Boolean, default=False)
    excerpt = Column(Text(500), default=False)
    creation_date = Column(DateTime, default=datetime.utcnow(), nullable=False)
    pub_date = Column(DateTime, nullable=True)
    url = Column(String(255), nullable=False, unique=True)


class AdminAccount(Base):
    __tablename__ = 'admin_accounts'
    id = Column(Integer, primary_key=True)
    identifier = Column(String(80), unique=True)
    password = Column(String)
    token = Column(String(500))

    def check_password(self, text):
        return self.password == crypt.crypt(text, self.password)

    def set_password(self, text):
        salt = ''.join([choice(SALT) for i in range(0, 32)]).encode('utf-8')
        salt = str(b64encode(salt + SECRET_PASS))
        self.password = crypt.crypt(text, '$6$'+salt)


class LoginAttempt(Base):
    __tablename__ = 'login_attempts'
    id = Column(Integer, primary_key=True)
    admin_id = Column(Integer)
    remote_address = Column(Text(46))
    date = Column(DateTime, default=datetime.utcnow(), nullable=False)
